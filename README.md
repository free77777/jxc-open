<!--
 * @Descripttion: 
 * @version: 
 * @Author: cxguo
 * @Date: 2019-10-30 16:13:59
 * @LastEditors: cxguo
 * @LastEditTime: 2020-12-24 22:32:24
-->
### 系统简介
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154627_c69b0bd9_2297150.png "屏幕截图.png")


> 官方网站: [http://www.b2zg.cn](http://www.b2zg.cn)

> 购买授权: 联系下方企业微信

> 版权归属: 如皋市韶光科技有限公司

|文档说明  |
|---------|
|[1. swagger接口文档](https://api.tao51d.com/doc.html) |
|[2. 版本更新日志](http://www.yuque.com/docs/share/199b52af-0fb3-461a-973b-4b353c8c9569)|

|演示地址  |
|---------|
| 1. 最新地址: http://124.71.61.105 |
| 2. 最新手机端体验地址：http://dev.b2zg.cn |
| 1. 网页电脑端演示 http://jxc.tao51d.com  |
| 3. 小程序![输入图片说明](http://img.tao51d.com/app_qr_code.jpg "屏幕截图.png")|
| 4. [安卓apk下载地址](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-2dfa8990-b883-4457-88a9-096aa64926bb/c4733695-26f9-493d-9b53-77e58145aebc.apk)|


|合作/授权/购置其他版本等商业行为|
|--------|
|![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/142527_1f80b16d_2297150.gif "企业微信.gif")|



```
2023-11-19
1、销售、进货模块可以录入金额，自动计算单价
2、修复若干bug

```

### 技术框架
后端
- 核心框架：SpringBoot 2.0.4.RELEASE
- 持久层框架：Mybatis, Mybatis-plus
- 数据库：Mysql 5.7.23
- 缓存：Redis
- 登录校验：Jwt-token Spring Security
- 单体应用：可自行Nginx负载均衡

前端
- JS框架：Vue 2.X
- UI框架：element-ui

硬件
- 蓝牙标签/小票打印机推荐: 芯烨热敏标签条码打印机.  型号：XP-271U

### 系统功能

![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154736_c8db287e_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154750_f260d4cf_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154754_0cbb76de_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154812_5f9a3fe5_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154823_64b00d40_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/154826_ba233c95_2297150.png "屏幕截图.png")
1. 基础资料

| 资料 | 描述  | 完成度 |
|------|---|-----|
| 商品 | 多规格，多单位  | 100% |
| 仓库 |   | 100%    |
| 门店 |   | 100%    |
| 结算账户 |   | 100%    |
| 员工/导购员 |   | 100%    |
| 客户/供应商 | 往来单位  | 100%    |

2. 基本单据功能：

| 单据类型 | 完成度  | 单据类型 | 完成度  | 单据类型 | 完成度  |
|------|---|-----|---|---|---|
| 进货订单 | 100%  | 进货单 | 100%  | 进货退货单 | 100%  |
| 销售订单 | 100%  |  销售单   |  100% | 销售退货单 | 100%  |
| 盘点单 |100%   | 调拨单    | 100%  | 组装拆卸单 | 100%  |
| 入库单 |  100% | 出库单     | 100%  | |   |
| 借入单 | 100%  | 借出单    | 100%  | |  |

3. 财务模块

| 单据类型 | 完成度  | 单据类型 | 完成度  | 单据类型 | 完成度  |
|------|---|-----|---|---|---|
| 应收款 | 100%  | 应付款 | 100%  | 日常收支 | 100%  |
| 账户转账 | 100%  |  核销   | 0%  | 账期管理 | 0%  |

4. 日志模块（资金流，库存流）

| 日志类型 | 完成度  | 日志类型 | 完成度  | 日志类型 | 完成度  |
|------|---|-----|---|---|---|
| 库存流水 | 100%  | 资金流水 | 100%  | 欠款流水 | 100%  |

5. 系统模块

| 类型 | 完成度  |
|------|---|
| 单据编号规则 | 100%  |
| 角色权限配置 | 100%  |
| 企业资料维护 | 100%  |
| 系统重置 | 100%  |
| 期初信息 | 100%  |


6. 特色功能


| 类型 | 描述 |完成度  |
|------|---|---|
| 单据打印模板自定义| 用户可自行定义打印模板  | 100% |
| 客户等级价 | 不同客户不同价格  | 100%    |
| SN序列号维护 | 适用于手机等高单价行业  |100%  |
| 保质期批次 | 适用于食品行业  |10%  |
| 商品展柜 | 可将您想要展示的商品给某客户 |0%  |
| 分享协作 | 可将单据分享给往来单位，推行无纸化办公 |98%  |

### 未来规划
1. 新增生产模块
2. 单据表单自定义
3. 门店 -> 修改为组织架构，数据结构修改为树状结构，方便做数据权限

### 业务说明

![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/143339_50b7650b_2297150.png "屏幕截图.png")


### 系统美图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0901/101934_9cac1973_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0901/101957_9ec225f2_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0901/102037_a4b4832e_2297150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0901/102101_d094dc12_2297150.png "屏幕截图.png")