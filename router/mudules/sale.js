/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-12-13 17:04:37
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 16:36:42
 */
import Layout from '@/layout'

const data = {
  path: '/sale',
  component: Layout,
  redirect: 'noRedirect',
  meta: { title: '销售', icon: 'sale' },
  children: [
    // 销售单
    {
      path: 'sale_add',
      component: () => import('@/views/bill/sale/sale/detail'),
      name: 'SaleAdd',
      hidden: true,
      props: (route) => ({ operation: 'add', billId: route.query.billId }),
      meta: { title: '新增销售单', pid: 'sale1', perm: 'sale:add' }
    },
    {
      path: 'sale_copyadd',
      name: 'SaleCopyAdd',
      hidden: true,
      props: (route) => ({ operation: 'copyadd', billId: route.query.billId }),
      component: () => import('@/views/bill/sale/sale/detail'),
      meta: { title: '复制新增销售单', pid: 'sale1', perm: 'sale:add' }
    },
    {
      path: 'sale_list',
      name: 'SaleList',
      component: () => import('@/views/bill/sale/sale/list.vue'),
      meta: { title: '销售单', add: true, pid: 'sale1', perm: 'sale:list' }
    },
    {
      path: 'sale_detail',
      component: () => import('@/views/bill/sale/sale/detail-view'),
      name: 'SaleDetail',
      hidden: true,
      props: (route) => ({ operation: 'view', billId: route.query.billId }),
      meta: { title: '销售单详情', pid: 'sale1', perm: 'sale:add' }
    },
    // 销售退货
    {
      path: 'salereturn_add',
      name: 'SaleReturnAdd',
      hidden: true,
      props: (route) => ({ operation: 'add' }),
      component: () => import('@/views/bill/sale/return-sale/detail-default.vue'),
      meta: { title: '新增销售退货单', pid: 'sale1', perm: 'sale_return:add' }
    },
    {
      path: 'salereturn_copyadd',
      name: 'SaleReturnCopyAdd',
      hidden: true,
      props: (route) => ({ operation: 'copyadd', billId: route.query.billId }),
      component: () => import('@/views/bill/sale/return-sale/detail.vue'),
      meta: { title: '复制新增销售退货单', pid: 'sale1', perm: 'sale_return:add' }
    },
    {
      path: 'salereturn_list',
      name: 'SaleReturnList',
      component: () => import('@/views/bill/sale/return-sale/list.vue'),
      meta: { title: '销售退货', add: true, pid: 'sale1', perm: 'sale_return:list' }
    },
    {
      path: 'salereturn_detail',
      name: 'SaleReturnDetail',
      hidden: true,
      props: (route) => ({ operation: 'view', billId: route.query.billId }),
      component: () => import('@/views/bill/sale/return-sale/detail-view.vue'),
      meta: { title: '销售退货详情', pid: 'sale1', perm: 'sale_return:list' }
    },
    {
      path: 'salereturnadd_hasrelation',
      name: 'SaleReturnAddHasRelation',
      hidden: true,
      props: (route) => ({ billId: route.query.billId }),
      component: () => import('@/views/bill/sale/return-sale/detail-trans.vue'),
      meta: { title: '新增销售退货单(关联)', pid: 'sale1', perm: 'sale_return:add' }
    },
    {
      path: 'salereturnadd_norelation',
      name: 'SaleReturnAddNoRelation',
      hidden: true,
      props: (route) => ({ operation: 'add' }),
      component: () => import('@/views/bill/sale/return-sale/detail.vue'),
      meta: { title: '新增销售退货单(不关联)', pid: 'sale1', perm: 'sale_return:add' }
    }
  ]
}

export default data
