/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-12-13 17:04:37
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-19 13:00:54
 */
import Layout from '@/layout'

export default {
  path: '/finance',
  component: Layout,
  redirect: 'noRedirect',
  meta: { title: '财务', icon: 'caiwuguanli' },
  children: [
    {
      path: 'comegodebtrece_list',
      name: 'ComegoDebtReceList',
      component: () => import('@/views/finance/comego/receipt/list'),
      meta: { title: '应收欠款', add: true, pid: 'finance1', perm: 'debt_rece:list' }
    },
    {
      path: 'comegodebtrece_add',
      name: 'ComegoDebtReceAdd',
      hidden: true,
      component: () => import('@/views/finance/comego/rece-bill/detail.vue'),
      props: (route) => ({ operation: 'add' }),
      meta: { title: '新增收款单', pid: 'finance1', perm: 'debt_rece:add' }
    },
    {
      path: 'comegodebt_rece_detail',
      name: 'ComegoDebtReceDetail',
      hidden: true,
      component: () => import('@/views/finance/comego/receipt/detail-log.vue'),
      props: (route) => ({ comegoId: route.query.comegoId, comegoName: route.query.comegoName }),
      meta: { title: '客户欠款明细', pid: 'finance1', perm: 'debt_rece:list' }
    },
    //
    {
      path: 'comegodebtpay_list',
      name: 'ComegoDebtPayList',
      component: () => import('@/views/finance/comego/payment/list'),
      meta: { title: '应付欠款', add: true, pid: 'finance1', perm: 'debt_pay:list' }
    },
    {
      path: 'comegodebtpay_add',
      name: 'ComegoDebtPayAdd',
      hidden: true,
      component: () => import('@/views/finance/comego/pay-bill/detail.vue'),
      props: (route) => ({ operation: 'add' }),
      meta: { title: '新增付款单', pid: 'finance1', perm: 'debt_pay:add' }
    },
    {
      path: 'comegodebt_payment_detail',
      name: 'ComegoDebtPaymentDetail',
      hidden: true,
      component: () => import('@/views/finance/comego/payment/detail-log.vue'),
      props: (route) => ({ comegoId: route.query.comegoId, comegoName: route.query.comegoName }),
      meta: { title: '供应商付款明细', pid: 'finance1', perm: 'debt_pay:list' }
    },
    {
      path: 'dailyincome_list',
      name: 'DailyincomeList',
      props: (route) => ({ currentTab: route.query.currentTab }),
      component: () => import('@/views/finance/daily/list.vue'),
      meta: { title: '日常收支', pid: 'finance2', perm: 'dayily:list' }
    },
    {
      path: 'capital_cat_list',
      name: 'CapitalCatList',
      component: () => import('@/views/finance/capital-cat/list.vue'),
      meta: { title: '收支项目', pid: 'finance2', perm: 'dayily:list' }
    },
    {
      path: 'account_trans_list',
      name: 'AccountTransList',
      component: () => import('@/views/finance/account-trans/list.vue'),
      meta: { title: '账户转账', pid: 'finance2', perm: 'account_trans:list' }
    },
    // {
    //   path: 'invoice_sale_list',
    //   name: 'InvoiceSaleList',
    //   component: () => import('@/views/bill/checkstock/list.vue'),
    //   meta: { title: '销售开票', pid: 'finance2', perm: 'account_trans:list' }
    // },
    {
      path: 'amountlog_list',
      name: 'AmountlogList',
      component: () => import('@/views/finance/amount-log/list.vue'),
      meta: { title: '资金流水', pid: 'finance3', perm: 'log_amount:list' }
    }
    // {
    //   path: 'performance_list',
    //   name: 'PerformanceList',
    //   component: () => import('@/views/finance/amount-log/list.vue'),
    //   meta: { title: '业绩提成', pid: 'finance3' }
    // }
  ]
}
