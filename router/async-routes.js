/*
 * @Descripttion: 动态路由
 * @version:
 * @Author: cxguo
 * @Date: 2019-09-08 11:11:06
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-07 13:36:55
 */
import Layout from '@/layout'
import saleData from './mudules/sale'
import stockData from './mudules/stock'
import financeData from './mudules/finance'
import reportData from './mudules/report'
import baseData from './mudules/basedata'
import setupData from './mudules/setup'

const data = [
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    meta: { title: '首页', icon: 'home' },
    children: [{
      path: 'home',
      name: 'Home',
      component: () => import('@/views/home/index'),
      meta: { title: '首页', icon: 'home' }
    }]
  },
  saleData,
  stockData,
  financeData,
  reportData,
  baseData,
  setupData,
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

export default data
