/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-07-24 08:18:49
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-20 09:36:33
 */

import { getUnitOptionsByUnitStr } from '../mixins/utils.js'
import { getPriceOptions } from '@/config/price.js'
import SelectUnit from '@/views/goods-select/SelectUnit'
import SelectPrice from '@/views/goods-select/SelectPrice'
import SelectSimGoodsList from '@/views/goods-select/SelectSimGoodsList'
import XEUtils from 'xe-utils'

export default (VXETable) => {
  VXETable.renderer.add('CxSelectGoods', {
    autofocus: 'input.el-input__inner',
    renderEdit(h, editRender, $table) {
      const { events, methods } = editRender
      const { row, column } = $table
      const tableProps = $table.$table.$parent.$attrs
      const methodsProps = methods || {}
      const cxprops = { ...tableProps, ...methodsProps }
      const data = {
        props: cxprops,
        model: {
          value: XEUtils.get(row, column.property),
          callback(value) {
            XEUtils.set(row, column.property, value)
          }
        },
        on: {
          'on-select-data': (data) => {
            const d = Object.assign($table, { data })
            events && events.onSelectData && events.onSelectData(d)
          }
        }
      }
      return [
        <SelectSimGoodsList {...data}/>
      ]
    },
    renderCell(h, cellRender, $table) {
      const { row, column } = $table
      const property = column.property
      const value = row[property]
      return [<span>{value}</span>]
    }
  })

  VXETable.renderer.add('CxSelectUnit', {
    autofocus: 'input.el-input__inner',
    renderCell(h, cellRender, $table) {
      const { row } = $table
      const { skuId, unitId } = row
      if (!skuId) return []
      const options = getUnitOptionsByUnitStr(row)
      const currentUnitObj = options.find(item => { return item.id === unitId }) || {}
      const unitName = currentUnitObj.name
      return [unitName]
    },
    renderEdit(h, editRender, $table) {
      const { row } = $table
      const { events } = editRender
      const { skuId } = row
      if (!skuId) return []
      const options = getUnitOptionsByUnitStr(row) || []
      const data = {
        props: { options },
        model: {
          value: XEUtils.get(row, 'unitId'),
          callback(value) {
            XEUtils.set(row, 'unitId', value)
          }
        },
        on: {
          'change': (data) => {
            const unitData = data
            events && events.change && events.change({ unitData, row })
          }
        }
      }
      return [<SelectUnit {...data}/>]
    }
  })

  VXETable.renderer.add('CxSelectPrice', {
    autofocus: 'input.el-input__inner',
    renderCell(h, cellRender, $table) {
      const { row } = $table
      const { price } = row
      return [price]
    },
    renderEdit(h, editRender, $table) {
      const { row, column } = $table
      const { skuId, priceStr } = row
      if (!skuId) return []
      const tableProps = $table.$table.$options.parent.$attrs
      const priceCat = tableProps['price-cat']
      const options = getPriceOptions(priceStr, priceCat)
      const defaultPriceCat = this.defaultPriceCat
      const data = {
        props: { options, priceCat, defaultPriceCat, row },
        model: {
          value: XEUtils.get(row, column.property),
          callback(value) {
            XEUtils.set(row, column.property, value)
          }
        }
      }
      return [<SelectPrice {...data}/>]
    }
  })
}
