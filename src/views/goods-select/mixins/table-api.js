/*
 * @Descripttion: 内部操作table的api
 * @version:
 * @Author: cxguo
 * @Date: 2019-10-18 15:37:24
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-15 15:10:50
 */

export default {
  data() {
    return { tableComp: null }
  },
  mounted() {
    this.tableComp = this.$refs.table
  },
  methods: {
    /**
     * 添加数据
     * @param {*} arrayData
     * @param {*} row
     */
    addTableData(arrayData, row = null) {
      const tableData = this.tableData
      tableData.push(...arrayData)
    },
    /**
     * 删除数据
     * @param {*} arrayData
     */
    delTableData(arrayData) {
      const _XIDS = arrayData.map(item => { return item._XID })
      const tableData = this.tableData
      for (let i = 0; i < tableData.length; i++) {
        const row = tableData[i]
        if (_XIDS.indexOf(row._XID) !== -1) {
          tableData.splice(i, 1)
          i--
        }
      }
    },
    /**
     * 修改表格数据
     * @param {原数据} sourceData
     * @param {目标数据} targetData
     */
    updateTableData(rowIndex, targetData) {
      const tableData = this.tableData
      const sourceData = tableData[rowIndex]
      Object.keys(targetData).forEach(key => {
        this.$set(sourceData, key, targetData[key])
      })
      if (this.isOpenTax) {
        const taxRate = this.taxRate
        sourceData.taxRate = taxRate
      }
    },
    /**
     * 获取表格数据
     */
    getTableData() {
      return this.tableData || []
    },
    /**
     * 获取增，删，改数据
     */
    getRecordset() {
      return this.$refs.table.getRecordset()
    },
    /**
     * 设置表格焦点
     * @param {*} row
     */
    setTableActiveRow(row) {
      const _this = this
      setTimeout(() => {
        _this.$refs.table.setActiveRow(row)
      }, 100)
    },
    setTableActiveCell(row, field) {
      const _this = this
      setTimeout(() => {
        _this.$refs.table.setSelectCell(row, field)
      }, 100)
    }
  }
}
