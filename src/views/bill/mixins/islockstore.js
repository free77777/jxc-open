/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 14:50:28
 */
import { getDataById } from '@/api/bsd/store.js'

export default {
  data() {
    return {}
  },
  watch: {
  },
  methods: {
    handleStoreLock() {
      getDataById().then(res => {
        if (res.data.flag) {
          const data = res.data.data
          if (data.isLock === '1') {
            this.popStoreMsg(data)
          }
        }
      })
    },
    popStoreMsg(data) {
      this.$alert('仓库正在盘点，解锁后才能进行出入库类业务操作！', '提示：', {
        confirmButtonText: '确定',
        callback: action => {
        }
      })
    }
  }
}

