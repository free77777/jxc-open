
/*
 * @Descripttion: 价格的变化
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-23 20:54:43
 */
export default {
  data() {
    return {}
  },
  computed: {
    'isOpenTax': function() {
      const isOpenTax = this.appData.businessSetup.isOpenPurchaseTax
      return isOpenTax === '1'
    },
    /**
     * 税率值
     */
    'taxRate': function() {
      return Number(this.appData.businessSetup.purchaseTax)
    },
    /**
     * 商品合计信息
     */
    totalMsg: function() {
      const GoodsSelect = this.goodsSelectComp
      if (!GoodsSelect) return {}
      return GoodsSelect.totalMsg
    },
    /**
     * 折扣额度
     */
    amountDiscount: function() {
      const { discountType, discountValue } = this.dataObj
      if (discountType === '0') {
        const goodsPrice = this.totalMsg.price || '0'
        const value = this.$amount(goodsPrice).multiply((100 - Number(discountValue)) / 100).format()
        return value
      }
      return discountValue
    },
    /**
     * 应收总额
     */
    amountPayable: function() {
      const { price, taxAmount } = this.totalMsg
      const goodsPrice = price || '0.00'
      const amountOther = this.dataObj.amountOther
      const amountDiscount = this.amountDiscount
      const amountPayable = this.$amount(goodsPrice).add(amountOther)
        .add(taxAmount).subtract(amountDiscount).format()
      return amountPayable
    },
    /**
     * 欠款
     */
    amountDebt: function() {
      const { amountPayable, amountPaid } = this.dataObj
      const value = this.$amount(amountPayable).subtract(amountPaid).format()
      return value
    },
    /**
     * 欠款总额
     * @param {} params
     */
    amountDebtTotal: function(params) {
      return this.amountDebt
    }
  },
  watch: {
    'amountPayable': {
      handler(data) {
        const amountPayable = data || '0.00'
        this.$set(this.dataObj, 'amountPayable', amountPayable)
        this.$set(this.dataObj, 'amountPaid', amountPayable)
      },
      immediate: true
    },
    'amountDebt': {
      handler(val) {
        this.$set(this.dataObj, 'amountDebt', val)
      }
    },
    'amountDiscount': {
      handler(val) {
        this.$set(this.dataObj, 'amountDiscount', val)
      }
    },
    'dataObj.disCountType': {
      handler(val) {
        const { discountValue } = this.dataObj
        const goodsPrice = this.totalMsg.price || '0'
        let value = ''
        if (val === '1') {
          const disCountPercent = this.$amount(100).subtract(discountValue).format()
          value = this.$amount(disCountPercent).divide(100).multiply(goodsPrice).format()
        } else {
          const realMoney = this.$amount(goodsPrice).subtract(discountValue).format()
          value = this.$amount(realMoney).divide(goodsPrice).multiply(100).format()
        }
        this.$set(this.dataObj, 'discountValue', value)
      }
    }
  },
  methods: {
  }
}

