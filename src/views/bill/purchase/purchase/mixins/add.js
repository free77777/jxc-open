
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-10 15:55:42
 */
import moment from 'moment'
import { getBillcode } from '@/api/bill/bill-purchase.js'

export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      const defaultPrice = this.$amount(0).format()
      this.dataObj = {
        businessTime: date,
        amountPayable: defaultPrice,
        amountPaid: defaultPrice,
        amountOther: defaultPrice,
        handUserId: this.currentUserId
      }
      this.getBillCode()
    },
    getBillCode() {
      getBillcode('purchase').then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

