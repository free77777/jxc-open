/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-04 12:37:50
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-11 20:26:09
 */
import { amount } from '@/libs/tools.js'

export const initDefaultStockWarnData = function(storeData) {
  return initStockWarnData(storeData)
}

export const initUpdateStockWarnData = function(goodsStockList) {
  return initStockWarnData(null, goodsStockList)
}

/**
 * 初始化stock数据
 */
function initStockWarnData(storeData, goodsStockList) {
  if (goodsStockList) {
    return goodsStockList
  }
  const zero = amount(0).format()
  return storeData.map(item => {
    return {
      minQtyWarning: zero,
      maxQtyWarning: zero
    }
  })
}
