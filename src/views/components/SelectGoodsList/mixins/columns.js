
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-21 13:23:39
 */

import { getBaseUnitByUnitStr } from '@/views/goods-select/mixins/utils.js'
import XEUtils from 'xe-utils'

export default {
  data() {
    return {
      defaultColumns: [
        { type: 'checkbox', width: 60, align: 'center' },
        { field: 'quantity', title: '数量', width: 160,
          slots: {
            default: (params) => {
              const { row, column } = params
              // eslint-disable-next-line no-unused-vars
              const h = this.$createElement
              const data = {
                props: { min: 0, size: 'mini' },
                model: {
                  value: XEUtils.get(row, column.property),
                  callback(value) {
                    XEUtils.set(row, column.property, value)
                  }
                },
                on: {
                  change: (e) => { this.onQtyChange(Object.assign(params, { $e: e })) },
                  blur: (e) => { this.onQtyBlur(Object.assign(params, { $e: e })) },
                  focus: (e) => { this.onQtyFocus(Object.assign(params, { $e: e })) }
                },
                nativeOn: {
                  click: (e) => { this.onQtyClick(Object.assign(params, { $e: e })) }
                }
              }
              return [<el-input-number {...data}/>]
            }
          }
        },
        { field: 'code', title: '商品编号', showOverflow: true, width: 140 },
        { field: 'name', title: '商品名称/规格', showOverflow: true, width: 200 },
        { field: 'unitNameStr', title: '基础单位', width: 80,
          slots: {
            default: (params) => {
              // eslint-disable-next-line no-unused-vars
              const h = this.$createElement
              const { row } = params
              const baseUnit = getBaseUnitByUnitStr(row)
              const value = baseUnit.unitName
              return [<span>{value}</span>]
            }
          }
        }
      ]
    }
  },
  methods: {
  }
}

