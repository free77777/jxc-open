/*
 * @Descripttion: 报表对比接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 10:38:23
 */
import axios from '@/utils/request'
const baseUrl = '/report/purchaseSaleCompare'

export function listCompare(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}
