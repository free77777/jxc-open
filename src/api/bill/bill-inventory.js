/*
 * @Descripttion: 盘点单据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 14:06:45
 */
import axios from '@/utils/request'
const baseUrl = '/bill/inventory'

/**
 * 添加盘点单
 * @param {*} params
 */
export function addInventory(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

export function getBillNo(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}

/**
 * 查询盘点单
 * @param {*} params
 */
export function listInventory(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getBillDetailById(billId) {
  return axios.request({
    url: `${baseUrl}/getBillDetails/${billId}`,
    method: 'post',
    data: billId
  })
}

export function print(params) {
  return axios.request({
    url: `${baseUrl}/print`,
    method: 'post',
    data: params
  })
}
