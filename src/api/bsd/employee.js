/*
 * @Descripttion: 账户
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-07 06:22:39
 */
import axios from '@/utils/request'
const baseUrl = '/employee'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delData(id) {
  return axios.request({
    url: `${baseUrl}/delete/${id}`,
    method: 'post',
    data: id
  })
}

export function updateEmployee(params) {
  return axios.request({
    url: `${baseUrl}/updateData`,
    method: 'post',
    data: params
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

/**
 * 模糊查询
 * @param {*} params
 */
export function options(params) {
  return axios.request({
    url: `${baseUrl}/listOptions`,
    method: 'post',
    data: params
  })
}

export function optionsIncludeGuide(params) {
  return axios.request({
    url: `${baseUrl}/listOptionsIncludeGuide`,
    method: 'post',
    data: params
  })
}

export function optionsCurrent(params) {
  return axios.request({
    url: `${baseUrl}/listOptionsByCurrentUser`,
    method: 'post',
    data: params
  })
}

export function getDataById(id) {
  return axios.request({
    url: `${baseUrl}/getData/${id}`,
    method: 'post',
    data: id
  })
}

