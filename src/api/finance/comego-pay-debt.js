/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 16:08:44
 */
import axios from '@/utils/request'
const baseUrl = '/fin/comegoPayDebt'

export function listDataGroupByComegoPage(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByComegoPage`,
    method: 'post',
    data: params
  })
}

export function listDetailByComegoId(params) {
  const comegoId = params.data.comegoId
  return axios.request({
    url: `${baseUrl}/listDebtDetail/${comegoId}`,
    method: 'post',
    data: params
  })
}

export function getStatistics(params) {
  return axios.request({
    url: `${baseUrl}/getStatistics`,
    method: 'post',
    data: params
  })
}

