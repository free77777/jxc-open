/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 15:34:45
 */
import axios from '@/utils/request'
const baseUrl = '/fin/comegoPayBill'

export function addData(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

export function getBillCode(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}
