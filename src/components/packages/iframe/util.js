/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-02-29 15:30:59
 * @LastEditors: cxguo
 * @LastEditTime: 2020-02-29 15:45:42
 */
import utils from '../utils/utils'

/**
  * 百分比换算成屏幕对应长宽 数值
  * @param {百分比数} perStr
  * @param {百分比数} type = w,h
  */
export const percentage2relArea = function(perStr, type) {
  const number = Number(perStr.replace('%', ''))
  const screenArea = utils.getDomArea(document.body)
  const m = screenArea[type]
  return m * (number / 100)
}
