/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-09-20 11:16:16
 * @LastEditors: cxguo
 * @LastEditTime: 2019-09-20 14:36:17
 */
export default {
  methods: {
    close () {
      this._close()
    },
    min () {
      this._min()
    },
    max () {
      this._max()
    }
  }
}
