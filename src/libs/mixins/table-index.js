/*
 * @Descripttion: 表格列
 * @version:
 * @Author: cxguo
 * @Date: 2019-03-28 14:09:53
 * @LastEditors: cxguo
 * @LastEditTime: 2019-10-29 17:12:04
 */
import { getColumnsOrderData } from '@/libs/util.js'
import SetColsDialog from '@/components/SetColumns'
import Vue from 'vue'

export default {
  data() {
    return {
      columnsOrderData: [] // 表格列顺序Data
    }
  },
  methods: {

    /**
     * 创建设置表格列的Dialog
     * @param {表格ID} tableId
     * @param {列数据} columnsData
     */
    createSetColsDialog({ tableId, columnsData }) {
      const Class = Vue.extend(SetColsDialog)
      const props = {
        colData: columnsData,
        tableId: tableId
      }
      let instance = Object.assign(new Class(), props)
      const dialog__wrappers = document.getElementsByClassName('el-dialog__wrapper')
      if (dialog__wrappers.length) {
        instance = instance.$mount('.el-dialog__wrapper')
      } else {
        instance = instance.$mount()
        document.body.appendChild(instance.$el)
      }
      return instance
    },

    /**
     * 获取用于显示的列数据
     * @param {列顺序Data} columnsOrderData
     * @param {原始列数据} originColumnsData
     */
    getShowColumnsData(columnsOrderData, originColumnsData) {
      if (!columnsOrderData) return originColumnsData
      const result = []
      columnsOrderData.forEach(item => {
        !item.hidden && result.push(originColumnsData[item.dataIndex])
      })
      return result
    },

    /**
     * 初始化表格缓存列数据
     * @param {表格Id} tableId
     * @param {表格组件} tableComp
     */
    initTableCacheColData({ tableId, tableComp }) {
      const columnsOrderData = getColumnsOrderData(tableId)
      this.columnsOrderData = columnsOrderData
      if (tableComp) {
        tableComp.resetResizable()
        tableComp.refreshData()
        tableComp.updateFooter()
      }
    }
  }
}

