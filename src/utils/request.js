/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-24 20:52:10
 */
import HttpRequest from '@/utils/axios'
import { getDomain } from '@/utils/system.js'

const domain = getDomain()
const axios = new HttpRequest(domain)
export default axios
